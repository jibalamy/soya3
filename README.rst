S O Y A   3 D
%%%%%%%%%%%%%

Intro
=====

Soya 3D is a high level 3D engine for Python ; it aims at being to 3D
what Python is to programming. Soya version 3 targets Python 3, as it
name (well, number) suggests.

It relies on OpenGL and SDL. It is designed with games in mind, and
written partly in Python and partly in Cython; our goal is to provide a
full architecture for making free (GPL) game of professional quality
entirely in Python.

Soya 3D requires:

- Python (tested with 3.4), including 'Python devel' Linux package for compiling
- OpenGL (possibly Mesa)
- SDL 2.0 (http://libsdl.org)
- GLEW (The OpenGL Extension Wrangler Library, http://glew.sf.net)
- Cal3D 0.10.0 (http://cal3d.sourceforge.net ; cal3d is sufficient, cal3d_viewer, data,... are not needed)
- Pillow (https://pypi.python.org/pypi/Pillow)
- libFreeType2 (http://www.freetype.org)
- Cerealizer (http://www.lesfleursdunormal.fr/static/informatique/cerealizer/index_en.html)
- OpenAL (http://openal.org) for sound support

Optional dependencies:

- Cython (tested with 0.20.1) (http://cython.org),
  for compiling Soya's for Mercurial repository. Source release already include the Cython-generated C files.

- ODE (0.10 or upper, http://www.ode.org/), if you want to use the physics engine -- still untested for Soya 3.


Installation
============

Cython is not needed to compile Soya from source release, since the C code
generated by Cython is already included in the source. 
However, Cython is needed to compile Soya from Mercurial repository.

Soya3D uses Python distutils for installation.
To install, type the following in a terminal::

  cd Soya3-XXX
  python3 ./setup.py build
  su # enter root password
  python3 ./setup.py install

Then you can test Soya as following::

  python3 ./tutorial/basic-1.py


Documentation
=============

Documentation available on Soya 3D includes:
 - The tutorial, in the tutorial/ directory
 - The doc strings present in some modules
 - The Yet-in(complete) Soya Handbook in the doc/ directory (not yet updated for Soya 3, sorry!).


Blender exporters
=================

The Blender exporter has been tested with Blender 2.70.
They include both Blender => Soya and Blender => Cal3D (for animated models) exporters.


Contact and links
=================

Enjoy Soya 3D ! And stop eating meat :-) !

Jiba -- Jean-Baptiste LAMY -- <jibalamy *@* free *.* fr>

Soya 3D website : http://www.lesfleursdunormal.fr/static/informatique/soya3d/index_en.html

Soya 3D on BitBucket (development repository): https://bitbucket.org/jibalamy/soya3
